# soap-now

A small library to make SOAP requests easier with Node.js
## Installation

```bash
npm install soap-now
```

## Requirements
  - Node.js >=7.6.0 (async/await support)

## Usage

### Node.js

```js
const soapRequest = require('soap-now');

// example data


## Changelog

[Changelog.md](CHANGELOG.md)

## Tests

