const soap = require('./lib/soap');

function soapXml(options) {
    return soap(options.baseUrl);
}
module.exports = {
    soapXml: soapXml
}