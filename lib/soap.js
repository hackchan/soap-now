const axios = require('axios')

module.exports = function getSoap(basUrl) {
    let header= {
        "User-Agent": "soap-now",
        "Cache-Control": "no-cache",
        "content-type": "application/xml;charset=utf-8",
        Accept: "*/*",
        "X-Custom-Header": "foobar"
        //'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
        //'Authorization':'Bearer ' + token,
        //'X-Requested-With': 'XMLHttpRequest'
      }
    
      const config = {
        baseURL: basUrl,
        timeout: 6000,
        header
    }
    
    const instance = axios.create(config);

    function post(path, body, headers, timeout) {
        return new Promise((resolve, reject)=>{
            let url = `${basUrl}${path}`;
            header = Object.assign({}, header, headers)
            instance.defaults.timeout = timeout;
            instance.post(url, body, header)
            .then((result)=>{
                result.data = result.data.replace(/(&lt;)/g, "<").replace(/(&gt;)/g, ">");
                resolve(result)
            })
            .catch((err)=>{
                reject(err)
            })
            ;
        })
    }
    return {
        post:post
    }
}
